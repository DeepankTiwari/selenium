package org.example;


import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;


public class Main {

    static ChromeDriver driver;
    public static String username = "8970510897";
    public static String password = "mnbvcxz321.";
    public static String website = "https://www.amazon.in";
    public static String searchProduct = "mobile";


    public static void main(String[] args){

        initiateBrowser();
        login();
        searchProduct(searchProduct);
        scrollAction();
        slideOverMenus();
        addToCart();
        buyProduct();

    }

    public static void initiateBrowser(){

        try {
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();
            driver.manage().window().maximize();
            driver.get(website);
        }
        catch (Exception error){
            System.out.println("Error in initiateBrowser function " + error);
        }
    }


    public static void login(){

        try {
            driver.findElement(By.id("nav-link-accountList")).click();
            driver.findElement(By.id("ap_email")).sendKeys(username);
            driver.findElement(By.id("continue")).click();
            driver.findElement(By.id("ap_password")).sendKeys(password);
            driver.findElement(By.id("signInSubmit")).click();
        }
        catch (Exception error){
            System.out.println("Error in login function " + error);
        }
    }




    public static void searchProduct(String product){

        try {
            driver.findElement(By.id("twotabsearchtextbox")).sendKeys(product);
            driver.findElement(By.id("nav-search-submit-button")).click();
        }
        catch (Exception error){
            System.out.println("Error in searchProduct function " + error);
        }
    }

    public static void scrollAction(){

        try {
            Actions action = new Actions(driver);
            action.scrollByAmount(0, 4000).perform();
        }
        catch (Exception error){
            System.out.println("Error in scrollAction function" + error);
        }
    }


    public static void slideOverMenus() {

        try {
            Actions action = new Actions(driver);

            WebElement navLogoElement = driver.findElement(By.id("nav-logo"));
            action.moveToElement(navLogoElement).click().build().perform();
            WebElement hamburgerMenuElement = driver.findElement(By.id("nav-hamburger-menu"));
            action.moveToElement(hamburgerMenuElement).click().build().perform();
            WebElement elementOne = driver.findElement(By.xpath("//a[@class='hmenu-item' and @data-menu-id='2']"));
            action.moveToElement(elementOne).click().build().perform();
            WebElement elementTwo = driver.findElement(By.xpath("//a[@class='hmenu-item' and text()='All-new Echo (4th Gen)']"));
            action.moveToElement(elementTwo).click().build().perform();
        }
        catch (Exception error){
            System.out.println("Error in slideOverMenus function " + error);
        }
    }


    public static void addToCart(){

        try {
            driver.findElement(By.id("add-to-cart-button")).click();
        }
        catch (Exception error){
            System.out.println("Error in addToCart function " + error);
        }
    }

    public static void buyProduct(){

        try {
            driver.findElement(By.id("nav-cart-count-container")).click();
            driver.findElement(By.id("sc-buy-box-ptc-button")).click();
            driver.findElement(By.xpath("//input[@class='a-button-input' and @aria-labelledby='shipToThisAddressButton-announce']")).click();
        }
        catch (Exception error){
            System.out.println("Error in buyProduct function " + error);
        }
    }

}